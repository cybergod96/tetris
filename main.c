#include "main.h"

void update_rotated_block()
{
	player_block_pattern = get_next_rotation();
	//update player_block_matrix
	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			player_block_matrix[j][i] = patterns[player_block_pattern][i][j];
		}
	}
}

void generate_player_block()
{
	player_block_type = next_block;
	next_block = rand() % 7 + 1;
	player_block_pattern = get_initial_block_pattern(player_block_type);
	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			player_block_matrix[j][i] = player_block_matrix[j][i] = patterns[player_block_pattern][i][j];
		}
	}
}

void fast_move_down()
{
	while (can_move_player_block(DIRECTION_DOWN))
	{
		player_block_y++;
	}
	place_player_block();
	reset_player_block();
	if (check_game_over())
		game_state = GS_GameOver;
}

bool can_move_player_block(int dir)
{	
	for (int i = player_block_x; i <= player_block_x + 3; i++)
	{
		for (int j = player_block_y; j <= player_block_y + 3; j++)
		{
			if (player_block_matrix[i - player_block_x][j - player_block_y])
			{
				if (dir != DIRECTION_DOWN )
				{
					if (dir == DIRECTION_LEFT)
					{
						if (i - 1 < 0) return false;
						else if (get_tile(i - 1, j)) return false;
					}
					else if (dir == DIRECTION_RIGHT)
					{
						if (i + 1 >= BOARD_WIDTH) return false;
						else if (get_tile(i + 1, j)) return false;
					}
				}
				else if (get_tile(i, j + 1) || j + 1 >= BOARD_HEIGHT)
				{
					return false;
				}
			}
		}
	}
	return true;
}

void place_player_block()
{
	for (int i = player_block_x; i <= player_block_x + 3; i++)
	{
		for (int j = player_block_y; j <= player_block_y + 3; j++)
		{
			if(player_block_matrix[i-player_block_x][j-player_block_y])
				set_tile(i, j, true);
		}
	}
}

int get_initial_block_pattern(int block_type)
{
	switch (block_type)
	{
	case 1: return 0; break;
	case 2: return 2; break;
	case 3: return 6; break;
	case 4: return 10; break;
	case 5: return 11; break;
	case 6: return 15; break;
	case 7: return 17; break;
	}
	return -1;
}

bool init()
{
	if (SDL_Init(SDL_INIT_EVERYTHING) == -1)
	{
		return false;
	}

	screen = SDL_SetVideoMode(SCREEN_WIDTH, SCREEN_HEIGHT, SCREEN_BPP, SDL_HWSURFACE);

	if (screen == NULL)
	{
		return false;
	}

	if (TTF_Init() == -1)
	{
		return false;
	}

	fps = malloc(sizeof(struct Timer));
	falling_time_timer = malloc(sizeof(struct Timer));
	game_state = GS_Menu;

	SDL_Rect r = { 300,200,192,FONT_MENU_OPTION_SIZE+8 };
	SDL_Color sc = { 127,127,127 };
	SDL_Color nc = { 153,153,0 };
	current_option = 0;
	MenuOption_Init(&options[0], "New Game", r, nc, sc);
	options[0].selected = true;
	r.y += r.h * 3;
	MenuOption_Init(&options[1], "Highscores", r, nc, sc);
	r.y += r.h * 3;
	MenuOption_Init(&options[2], "Exit", r, nc, sc);
	for (int i = 0; i < MENU_OPTIONS_COUNT; i++)
	{
		MenuOption_UpdateBg(&options[i]);
	}

	next_block = rand() % 7 + 1;
	reset_player_block();
	snprintf(txt_points, sizeof(txt_points), "Points: %d", points);
	snprintf(txt_level, sizeof(txt_level), "Level: %d", level);

	FILE *f = fopen("highscores", "r");
	if(!f)
	{
		for (int i = 0; i < 10; i++)
		{
			highscores[i] = 0;
		}
	}
	else
	{
		for (int i = 0; i < 10; i++)
		{
			fscanf(f, "%d", &highscores[i]);
		}
		fclose(f);
	}

	board = malloc(BOARD_WIDTH*BOARD_HEIGHT * sizeof(bool));
	for (int i = 0; i < BOARD_WIDTH; i++)
	{
		for (int j = 0; j < BOARD_HEIGHT; j++)
		{
			set_tile(i, j, false);
		}
	}

	SDL_WM_SetCaption("Tetris", NULL);

	return true;
}

bool load_files()
{
	block_filled = load_image("block.png");
	block_empty = load_image("block2.png");

	font = TTF_OpenFont("times.ttf", 24);
	font_menu_option = TTF_OpenFont("times.ttf", FONT_MENU_OPTION_SIZE);

	if (!block_filled)
	{
		fprintf(stderr, "Could not load file 'block.png'.");
		return false;
	}
	if (!block_empty)
	{
		fprintf(stderr, "Could not load file 'block2.png'.");
		return false;
	}
	if (font == NULL)
	{
		return false;
	}
	return true;
}

void clean_up()
{
	MenuOption_Deinit(&options[0]);
	MenuOption_Deinit(&options[1]);
	MenuOption_Deinit(&options[2]);
	SDL_FreeSurface(block_filled);
	SDL_FreeSurface(block_empty);

	free(fps);
	free(falling_time_timer);
	free(board);

	TTF_CloseFont(font);
	TTF_CloseFont(font_menu_option);
	TTF_Quit();
	SDL_Quit();
}

void set_tile(int x, int y, bool t)
{
	board[y*BOARD_WIDTH + x] = t;
}

bool get_tile(int x, int y)
{
	return board[y*BOARD_WIDTH + x];
}

int get_next_rotation()
{
	switch (player_block_type)
	{
	case 1:
		return (player_block_pattern == 0) ? 1 : 0;
		break;
	case 2:
		if (player_block_pattern + 1 > 5) return 2;
		else return player_block_pattern+1;
		break;
	case 3:
		if (player_block_pattern + 1 > 9) return 6;
		else return player_block_pattern+1;
		break;
	case 4: return 10; break;
	case 5:
		if (player_block_pattern + 1 > 14) return 11;
		else return player_block_pattern+1;
		break;
	case 6:
		return (player_block_pattern == 15) ? 16 : 15;
		break;
	case 7:
		return (player_block_pattern == 17) ? 18 : 17;
		break;
	}
}

bool is_line_full(int index)
{
	for (int i = 0; i < BOARD_WIDTH; i++)
	{
		if (!get_tile(i, index))
			return false;
	}
	return true;
}

bool is_line_empty(int index)
{
	for (int i = 0; i < BOARD_WIDTH; i++)
	{
		if (get_tile(i, index))
			return false;
	}
	return true;
}

bool can_rotate()
{
	int next_rotation = get_next_rotation();
	for (int i = player_block_x; i <= player_block_x + 3; i++)
	{
		for (int j = player_block_y; j <= player_block_y + 3; j++)
		{
			if (patterns[next_rotation][i - player_block_x][j - player_block_y])
			{
				if (player_block_x < 0 || player_block_x >= 7 || get_tile(i,j)) return false;
			}
		}
	}
	return true;
}

bool check_game_over()
{
	for (int i = player_block_x; i <= player_block_x + 3; i++)
	{
		for (int j = player_block_y; j <= player_block_y + 3; j++)
		{
			if (player_block_matrix[i - player_block_x][j - player_block_y] &&
				get_tile(i, j))
				return true;
		}
	}
	return false;
}

void select_option(int n)
{
	for (int i = 0; i < MENU_OPTIONS_COUNT; i++)
	{
		if (i == n) options[i].selected = true;
		else options[i].selected = false;
		MenuOption_UpdateBg(&options[i]);
	}
}

void add_points(int n)
{
	points += n;
	//update points text
	snprintf(txt_points, sizeof(txt_points), "Points: %d", points);
}

void reset_player_block()
{
	player_block_x = BOARD_WIDTH / 2 - 2;
	player_block_y = 0;
	generate_player_block();
}

void rotate()
{
	if (can_rotate())update_rotated_block();
	else
	{
		if (player_block_x < 0)
		{
			switch (player_block_type)
			{
			case 1:
				if (can_move_player_block(DIRECTION_RIGHT))
				{
					player_block_x++;
					update_rotated_block();
				}
				break;
			case 2:
				switch (player_block_pattern)
				{
				case 2:
					if (can_move_player_block(DIRECTION_RIGHT))
					{
						player_block_x++;
						update_rotated_block();
					}
					break;
				case 4: case 5:
					update_rotated_block();
					break;
				}
				break;
			case 3:
				switch (player_block_pattern)
				{
				case 6:
					if (can_move_player_block(DIRECTION_RIGHT))
					{
						player_block_x++;
						update_rotated_block();
					}
					break;
				case 8: case 9:
					update_rotated_block();
					break;
				}
				break;
			case 4:
				break;
			case 5:
				switch (player_block_pattern)
				{
				case 12: case 14:
					if (can_move_player_block(DIRECTION_RIGHT))
					{
						player_block_x++;
						update_rotated_block();
					}
					break;
				}
				break;
			case 6:
				switch (player_block_pattern)
				{
				case 16:
					if (can_move_player_block(DIRECTION_RIGHT))
					{
						player_block_x++;
						update_rotated_block();
					}
					break;
				}
				break;
			case 7:
				update_rotated_block();
				break;
			}

		}
		if (player_block_x >= 7)
		{
			int delta = 0, bx = player_block_x;
			bool cr = true;
			switch (player_block_type)
			{
			case 1:
				delta = player_block_x - 6;
				for (int i = 0; i < delta; i++)
				{
					if (can_move_player_block(DIRECTION_LEFT))
					{
						player_block_x--;
					}
					else
					{
						player_block_x = bx;
						cr = false;
					}
				}
				if (cr)
				{
					player_block_x = bx - delta;
					update_rotated_block();
				}
				break;
			case 2:
				switch (player_block_pattern)
				{
				case 2: case 3:
					update_rotated_block();
					break;
				case 4:
					if (can_move_player_block(DIRECTION_LEFT))
						player_block_x--;
					update_rotated_block();
					break;
				}
				break;
			case 3:
				switch (player_block_pattern)
				{
				case 6: case 7:
					update_rotated_block();
					break;
				case 8:
					if (can_move_player_block(DIRECTION_LEFT))
						player_block_x--;
					update_rotated_block();
					break;
				}
				break;
			case 4:
				break;
			case 5:
				switch (player_block_pattern)
				{
				case 11: case 12: case 13: case 14:
					update_rotated_block();
					break;
				}
				break;
			case 6:
				switch (player_block_pattern)
				{
				case 15: case 16:
					update_rotated_block();
					break;
				}
				break;
			case 7:
				switch (player_block_pattern)
				{
				case 18:
					if (can_move_player_block(DIRECTION_LEFT))
					{
						player_block_x--;
						update_rotated_block();
					}
					break;
				}
				break;
			}
		}
	}
}

void Game_Events()
{
	if (event.type == SDL_KEYDOWN)
	{
		switch (event.key.keysym.sym)
		{
		case SDLK_DOWN:
			fast_move_down();
			Timer_Start(falling_time_timer);
			break;
		case SDLK_UP:
			//TODO: wstawi� to wszystko do funkcji rotate_player_block()
			rotate();
			break;
		case SDLK_LEFT:
			if (can_move_player_block(DIRECTION_LEFT))
				player_block_x--;
			break;
		case SDLK_RIGHT:
			if (can_move_player_block(DIRECTION_RIGHT))
				player_block_x++;
			break;
		}
	}
}

void Game_Logic()
{
	if (!first_timer_start)
	{
		Timer_Start(falling_time_timer);
		first_timer_start = true;
	}

	//update boart after FALLING_TIME
	if (Timer_GetTicks(falling_time_timer) >= falling_time/* && !debug*/)
	{

		//move player block after checking
		if (can_move_player_block(DIRECTION_DOWN)) player_block_y++;
		else
		{
			place_player_block();
			reset_player_block();
			if (check_game_over())
				game_state = GS_GameOver;
		}

		//check if any line is full
		for (int i = 0; i < BOARD_HEIGHT; i++)
		{
			if (is_line_full(i))
			{
				//remove line
				for (int x = 0; x < BOARD_WIDTH; x++)
				{
					set_tile(x, i, false);
				}

				//move the rest of the board down
				for (int y = i - 1; y >= 0; y--)
				{
					for (int x = 0; x < BOARD_WIDTH; x++)
					{
						if (get_tile(x, y) && (!get_tile(x, y + 1) && y + 1 < BOARD_HEIGHT))
						{
							set_tile(x, y + 1, true);
							set_tile(x, y, false);
						}
					}
				}
				add_points(25);
				if (points >= next_level_threshold)
				{
					level++;
					falling_time -= 50;
					next_level_threshold += THRESHOLD_INCREASE;
					snprintf(txt_level, sizeof(txt_level), "Level: %d", level);
				}
			}
		}
		Timer_Start(falling_time_timer);
	}
}

void Game_Drawing()
{
	//display board and player block
	for (int i = 0; i < BOARD_WIDTH; i++)
	{
		for (int j = 0; j < BOARD_HEIGHT; j++)
		{
			if (get_tile(i, j) )
				apply_surface(i * (TILE_SIZE + TILE_SPACE) + BOAR_POS_X, j * (TILE_SIZE + TILE_SPACE) + BOAR_POS_Y, block_filled, screen, NULL);
			else
				apply_surface(i * (TILE_SIZE + TILE_SPACE) + BOAR_POS_X, j * (TILE_SIZE + TILE_SPACE) + BOAR_POS_Y, block_empty, screen, NULL);

			if ((i >= player_block_x && i <= player_block_x + 3 &&
				j >= player_block_y && j <= player_block_y + 3))

			{
				if (player_block_matrix[i - player_block_x][j - player_block_y])
				{
					apply_surface(i * (TILE_SIZE + TILE_SPACE) + BOAR_POS_X, j * (TILE_SIZE + TILE_SPACE) + BOAR_POS_Y, block_filled, screen, NULL);
				}
				//else
				//{
				//	if(debug)
				//	{
				//		SDL_Surface *s = SDL_CreateRGBSurface(0, TILE_SIZE, TILE_SIZE, 32, 0, 0, 0, 0);
				//		/*if (is_matrix_col_empty(1))*/SDL_FillRect(s, NULL, SDL_MapRGB(s->format, 255, 255, 0));
				//		//else SDL_FillRect(s, NULL, SDL_MapRGB(s->format, 255, 0, 0));
				//		apply_surface(i * (TILE_SIZE + TILE_SPACE) + BOAR_POS_X, j * (TILE_SIZE + TILE_SPACE) + BOAR_POS_Y, s, screen, NULL);
				//		SDL_FreeSurface(s);
				//	}
				//}
			}
		}
	}
	//game info
	show_text(0, 0, txt_points, text_color, screen, font, 0, 0, 0);
	show_text(0, 30, txt_level, text_color, screen, font, 0, 0, 0);
	//next block
	show_text(NEXT_BLOCK_POS_X, NEXT_BLOCK_POS_Y - 50, "Next block:", text_color, screen, font, 0, 0, 0);
	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			if (patterns[get_initial_block_pattern(next_block)][j][i])
			{
				apply_surface(NEXT_BLOCK_POS_X + i*(TILE_SIZE + TILE_SPACE), NEXT_BLOCK_POS_Y + j * (TILE_SIZE + TILE_SPACE), block_filled, screen, NULL);
			}
		}
	}
}

void Menu_Events()
{
	if (event.type == SDL_KEYDOWN)
	{
		if (event.key.keysym.sym == SDLK_DOWN)
		{
			if (current_option + 1 < 3) current_option++;			
			select_option(current_option);
		}
		else if (event.key.keysym.sym == SDLK_UP)
		{
			if (current_option - 1 >= 0) current_option--;
			select_option(current_option);
		}
		else if (event.key.keysym.sym == SDLK_RETURN)
		{
			switch (current_option)
			{
			case 0: game_state = GS_Game; break;
			case 1: game_state = GS_Highscores; break;
			case 2: quit = true; break;
			default: break;
			}
		}
		else if (event.key.keysym.sym == SDLK_x)
		{
			options[0].selected = !options[0].selected;
			MenuOption_UpdateBg(&options[0]);
		}
	}
}

void Menu_Drawing()
{
	for (int i = 0; i < MENU_OPTIONS_COUNT; i++)
	{
		MenuOption_Draw(&options[i],screen,text_color,font_menu_option);
	}
}

void Menu_Logic()
{
	for (int i = 0; i < MENU_OPTIONS_COUNT; i++)
	{
		MenuOption_UpdateBg(&options[i]);
	}
}

void GameOver_Events()
{
	if (event.type == SDL_KEYDOWN && event.key.keysym.sym == SDLK_RETURN)
	{
		//reseet board and player's block
		for (int i = 0; i < BOARD_WIDTH; i++)
		{
			for (int j = 0; j < BOARD_HEIGHT; j++)
			{
				set_tile(i, j, false);
			}
		}
		//insert score to highscores if needed
		for (int i = 0; i < 10; i++)
		{
			if (highscores[i] <= points)
			{
				for (int j = 9; j >= i; j--)
				{
					highscores[j] = highscores[j - 1];
				}
				highscores[i] = points;
				break;
			}
		}
		points = 0;	
		add_points(0);
		level = 1;
		next_level_threshold = INITIAL_THRESHOLD;
		reset_player_block();
		first_timer_start = false;
		game_state = GS_Menu;
	}
}

void GameOver_Drawing()
{
	show_text(0, 200, "GAME OVER!", text_color, screen, font, SCREEN_WIDTH, 0, Center_Horizontal);
	show_text(0, 250, txt_points, text_color, screen, font, SCREEN_WIDTH, 0, Center_Horizontal);
	show_text(0, 300, "Press ENTER to retun to main menu...", text_color, screen, font, SCREEN_WIDTH, 0, Center_Horizontal);
}

void GameOver_Logic()
{
	return;
}

void Highscores_Drawing()
{
	char entry[64];
	for (int i = 0; i < 10; i++)
	{
		snprintf(entry, sizeof(entry), "%d. %d", i+1, highscores[i]);
		show_text(300, 50 + i * 30, entry, text_color, screen, font, 0, 0, 0);
	}

	show_text(0, 400, "Press ENTER to return to main menu...", text_color, screen, font, SCREEN_WIDTH, 0, Center_Horizontal);
}

void Highscores_Events()
{
	if (event.type == SDL_KEYDOWN && event.key.keysym.sym == SDLK_RETURN)
	{
		game_state = GS_Menu;
	}
}

void Highscores_Logic()
{
	return;
}

int main(int argc, char* args[])
{
	srand(time(NULL));

	//bool quit = false;

	if (init() == false)
	{
		fprintf(stderr, "Initialization failed!\r\n");
		return 1;
	}

	if (load_files() == false)
	{
		return 1;
	}

	while (!quit)
	{
		SDL_FillRect(screen, NULL, 0);
		Timer_Start(fps);
		if (SDL_PollEvent(&event))
		{
			switch (game_state)
			{
			case GS_Menu: Menu_Events(); break;
			case GS_Game: Game_Events(); break;
			case GS_GameOver: GameOver_Events(); break;
			case GS_Highscores: Highscores_Events(); break;
			default: break;
			}
			if (event.type == SDL_QUIT)
			{
				quit = true;
			}
		}
		
		//logic
		switch (game_state)
		{
		case GS_Menu: Menu_Logic(); break;
		case GS_Game: Game_Logic(); break;
		case GS_GameOver: GameOver_Logic(); break;
		case GS_Highscores: Highscores_Logic(); break;
		default: break;
		}
		//drawing
		switch (game_state)
		{
		case GS_Menu: Menu_Drawing(); break;
		case GS_Game: Game_Drawing(); break;
		case GS_GameOver: GameOver_Drawing(); break;
		case GS_Highscores: Highscores_Drawing(); break;
		default: break;
		}

		if (SDL_Flip(screen) == -1)
		{
			return 1;
		}

		if (Timer_GetTicks(fps) < 1000 / FRAMES_PER_SECOND)
		{
			SDL_Delay((1000 / FRAMES_PER_SECOND) - Timer_GetTicks(fps));
		}
	}
	
	//save highscores
	FILE *f = fopen("highscores", "w");
	if (!f)
	{
		perror("Could not create highscores file!");
	}
	for (int i = 0; i < 10; i++)
	{
		fprintf(f, "%d\n", highscores[i]);
	}
	fclose(f);
	clean_up();

	return 0;
}
