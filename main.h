#pragma once
#include <stdbool.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <SDL.h>
#include <SDL_image.h>
#include <SDL_ttf.h>
#include "timer.h"
#include "functions.h"
#include "menu_option.h"

#define SCREEN_WIDTH 800
#define SCREEN_HEIGHT 600
#define SCREEN_BPP 32
#define FRAMES_PER_SECOND 120

#define DIRECTION_DOWN 0
#define DIRECTION_LEFT 1
#define DIRECTION_RIGHT 2

#define INITIAL_FALLING_TIME 650
#define INITIAL_THRESHOLD 1000
#define THRESHOLD_INCREASE 1000
#define BOARD_WIDTH 10
#define BOARD_HEIGHT 20
#define TILE_SIZE 16
#define TILE_SPACE 1
#define BOAR_POS_X 100
#define BOAR_POS_Y 150
#define NEXT_BLOCK_POS_X 400
#define NEXT_BLOCK_POS_Y 150


#define FONT_MENU_OPTION_SIZE 30

#define MENU_OPTIONS_COUNT 3

bool patterns[19][4][4] =
{
	//d�ugi (2)
	{
		{ 0,1,0,0 },
		{ 0,1,0,0 },
		{ 0,1,0,0 },//0
		{ 0,1,0,0 },
	},
	{
		{ 0,0,0,0 },
		{ 0,0,0,0 },
		{ 0,0,0,0 },//1
		{ 1,1,1,1 },
	},
	//odwr�cone L (4)
	{
		
		{ 0,0,1,0 },
		{ 0,0,1,0 },//2
		{ 0,1,1,0 },
		{ 0,0,0,0 },
	},
	{
		
		{ 1,0,0,0 },//3
		{ 1,1,1,0 },
		{ 0,0,0,0 },
		{ 0,0,0,0 },
	},
	{
		
		{ 0,1,1,0 },
		{ 0,1,0,0 },//4
		{ 0,1,0,0 },
		{ 0,0,0,0 },
	},
	{
		
		{ 0,1,1,1 },//5
		{ 0,0,0,1 },
		{ 0,0,0,0 },
		{ 0,0,0,0 },
	},
	//L (4)
	{
		
		{ 0,1,0,0 },
		{ 0,1,0,0 },//6
		{ 0,1,1,0 },
		{ 0,0,0,0 },
	},
	{
		
		{ 1,1,1,0 },//7
		{ 1,0,0,0 },
		{ 0,0,0,0 },
		{ 0,0,0,0 },
	},
	{
		
		{ 0,1,1,0 },
		{ 0,0,1,0 },//8
		{ 0,0,1,0 },
		{ 0,0,0,0 },
	},
	{
		
		{ 0,0,0,1 },//9
		{ 0,1,1,1 },
		{ 0,0,0,0 },
		{ 0,0,0,0 },
	},
	//kwadrat (1)
	{
		
		{ 0,1,1,0 },//10
		{ 0,1,1,0 },
		{ 0,0,0,0 },
		{ 0,0,0,0 },
	},
	//schodek (4)
	{
		
		{ 0,1,0,0 },//11
		{ 1,1,1,0 },
		{ 0,0,0,0 },
		{ 0,0,0,0 },
	},
	{
		
		{ 0,1,0,0 },
		{ 0,1,1,0 },//12
		{ 0,1,0,0 },
		{ 0,0,0,0 },
	},
	{
		
		{ 1,1,1,0 },//13
		{ 0,1,0,0 },
		{ 0,0,0,0 },
		{ 0,0,0,0 },
	},
	{
		
		{ 0,0,1,0 },
		{ 0,1,1,0 },//14
		{ 0,0,1,0 },
		{ 0,0,0,0 },
	},
	//z (2)
	{
		
		{ 1,1,0,0 },//15
		{ 0,1,1,0 },
		{ 0,0,0,0 },
		{ 0,0,0,0 },
	},
	{
		
		{ 0,0,1,0 },
		{ 0,1,1,0 },//16
		{ 0,1,0,0 },
		{ 0,0,0,0 },
	},
	//s (2)
	{
		
		{ 0,0,1,1 },//17
		{ 0,1,1,0 },
		{ 0,0,0,0 },
		{ 0,0,0,0 },
	},
	{
		
		{ 0,1,0,0 },
		{ 0,1,1,0 },//18
		{ 0,0,1,0 },
		{ 0,0,0,0 },
	},
};

MenuOption options[MENU_OPTIONS_COUNT];
int current_option;

typedef enum { GS_Menu, GS_Game, GS_GameOver, GS_Highscores } Game_State;

int player_block_x, player_block_y, player_block_type, player_block_pattern;
bool player_block_matrix[4][4];
int next_level_threshold = 500, level = 1, falling_time = INITIAL_FALLING_TIME, next_block;
Game_State game_state;

SDL_Surface *block_filled = NULL;
SDL_Surface *block_empty = NULL;
SDL_Surface *screen = NULL;

struct Timer *fps = NULL;
struct Timer *falling_time_timer = NULL;

bool *board = NULL, first_timer_start = false, quit = false;
int points = 0;
char txt_points[256], txt_level[32];
int highscores[10];

SDL_Event event;
TTF_Font *font = NULL, *font_menu_option = NULL;
SDL_Color text_color = { 255, 255, 255 };

void set_tile(int x, int y, bool t);
bool get_tile(int x, int y);
void update_rotated_block();
void generate_player_block();
void place_player_block();
bool can_move_player_block(int dir);
int get_next_rotation();
int get_initial_block_pattern(int block_type);
bool can_rotate();
bool is_line_full(int index);
bool is_line_empty(int index);
bool check_game_over();
void fast_move_down();
void add_points(int n);
void reset_player_block();
void select_option(int n);

void Menu_Events();
void Menu_Drawing();
void Menu_Logic();

void Game_Events();
void Game_Drawing();
void Game_Logic();

void GameOver_Events();
void GameOver_Drawing();
void GameOver_Logic();

void Highscores_Events();
void Highscores_Drawing();
void Highscores_Logic();
