#include "functions.h"

SDL_Surface *load_image(const char *filename)
{
	//The image that's loaded
	SDL_Surface* loadedImage = NULL;

	//The optimized surface that will be used
	SDL_Surface* optimizedImage = NULL;

	//Load the image
	loadedImage = IMG_Load(filename);

	//If the image loaded
	if (loadedImage != NULL)
	{
		//Create an optimized surface
		optimizedImage = SDL_DisplayFormat(loadedImage);

		//Free the old surface
		SDL_FreeSurface(loadedImage);

		//If the surface was optimized
		if (optimizedImage != NULL)
		{
			//Color key surface
			SDL_SetColorKey(optimizedImage, SDL_SRCCOLORKEY, SDL_MapRGB(optimizedImage->format, 0, 0xFF, 0xFF));
		}
	}

	//Return the optimized surface
	return optimizedImage;
}


void apply_surface(int x, int y, SDL_Surface* source, SDL_Surface* destination, SDL_Rect* clip)
{
	//Holds offsets
	SDL_Rect offset;

	//Get offsets
	offset.x = x;
	offset.y = y;

	//Blit
	SDL_BlitSurface(source, clip, destination, &offset);
}

void show_text(int x, int y, const char *text, SDL_Color color, SDL_Surface *destination, TTF_Font *font, int center_rect_w, int center_rect_h, TextFlags flags)
{
	SDL_Surface *txt_surface = TTF_RenderText_Blended(font, text, color);
	if(!center_rect_w && !center_rect_h)
		apply_surface(x, y, txt_surface, destination, NULL);
	else
	{
		//TODO: poprawyć wyśrodkowywanie tekstu
		int pos_x = x, pos_y = y;
		if (flags & Center_Horizontal) pos_x += (center_rect_w - txt_surface->w) / 2;
		if (flags & Center_Vertical) pos_y +=  (center_rect_h - txt_surface->h) / 2;
		apply_surface(pos_x, pos_y, txt_surface, destination, NULL);
	}
	SDL_FreeSurface(txt_surface);
}