#pragma once
#include <stdbool.h>
#include <SDL.h>
#include <SDL_image.h>
#include <SDL_ttf.h>
#include "functions.h"

#define MAX_OPTION_NAME_LENGTH 32

typedef struct {
	char name[MAX_OPTION_NAME_LENGTH];
	bool selected;
	SDL_Rect rect;
	SDL_Surface *bg;
	SDL_Color normal_color, selected_color;
} MenuOption;

void MenuOption_Init(MenuOption *o, char name[MAX_OPTION_NAME_LENGTH], SDL_Rect rect, SDL_Color normal_color, SDL_Color selected_color);
void MenuOption_Deinit(MenuOption *o);
void MenuOption_Draw(MenuOption *o, SDL_Surface *dst, SDL_Color font_color, TTF_Font *font);
void MenuOption_UpdateBg(MenuOption *o);