#pragma once
#include <stdbool.h>
struct Timer
{
	int startTicks, pausedTicks;
	bool started, paused;
};

void Timer_Start(struct Timer *);
void Timer_Stop(struct Timer *);
void Timer_Pause(struct Timer *);
void Timer_Unpause(struct Timer *);
int Timer_GetTicks(struct Timer *);
