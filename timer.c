#include <SDL.h>
#include "timer.h"

//void Timer::start()
//{
//	started = true;
//	paused = false;
//	startTicks = SDL_GetTicks();
//}
//
//void Timer::stop()
//{
//	started = false;
//	paused = false;
//}
//
//void Timer::pause()
//{
//	if ((started == true) && (paused == false))
//	{
//		paused = true;
//		pausedTicks = SDL_GetTicks() - startTicks;
//	}
//}
//
//void Timer::unpause()
//{
//	if (paused == true)
//	{
//		paused = false;
//		startTicks = SDL_GetTicks() - pausedTicks;
//		pausedTicks = 0;
//	}
//}
//
//int Timer::get_ticks()
//{
//	if (started == true)
//	{
//		if (paused == true)
//		{
//			return pausedTicks;
//		}
//		else
//		{
//			return SDL_GetTicks() - startTicks;
//		}
//	}
//	return 0;
//}

void Timer_Start(struct Timer *t)
{
	t->started = true;
	t->paused = false;
	t->startTicks = SDL_GetTicks();
}

void Timer_Stop(struct Timer *t)
{
	t->started = false;
	t->paused = false;
}

void Timer_Pause(struct Timer *t)
{
	if (t->started && !t->paused)
	{
		t->paused = true;
		t->pausedTicks = SDL_GetTicks() - t->startTicks;
	}
}

void Timer_Unpause(struct Timer *t)
{
	if (t->paused)
	{
		t->paused = false;
		t->startTicks = SDL_GetTicks() - t->pausedTicks;
		t->pausedTicks = 0;
	}
}

int Timer_GetTicks(struct Timer *t)
{
	if (t->started)
	{
		if (t->paused) return t->pausedTicks;
		else return SDL_GetTicks() - t->startTicks;
	}
	return 0;
}