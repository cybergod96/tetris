#pragma once
#include <SDL.h>
#include <SDL_image.h>
#include <SDL_ttf.h>

typedef enum {Center_Horizontal = 1, Center_Vertical = 2} TextFlags;

SDL_Surface *load_image(const char *filename);
void apply_surface(int x, int y, SDL_Surface* source, SDL_Surface* destination, SDL_Rect* clip);
void show_text(int x, int y, const char *text, SDL_Color color, SDL_Surface *destination, TTF_Font *font, int center_rect_w, int center_rect_h, TextFlags flags);
