#include "menu_option.h"

void MenuOption_Draw(MenuOption *o, SDL_Surface *dst, SDL_Color font_color, TTF_Font *font)
{
	MenuOption_UpdateBg(o);
	apply_surface(o->rect.x, o->rect.y, o->bg, dst, NULL);
	show_text(o->rect.x, o->rect.y, o->name, font_color, dst, font, o->rect.w, o->rect.h, Center_Horizontal | Center_Vertical);
}

void MenuOption_Init(MenuOption *o, char name[MAX_OPTION_NAME_LENGTH], SDL_Rect rect, SDL_Color normal_color, SDL_Color selected_color)
{
	strcpy(o->name, name);
	o->rect = rect;
	o->selected = false;
	o->selected_color = selected_color;
	o->normal_color = normal_color;
	o->bg = SDL_CreateRGBSurface(0, rect.w, rect.h, 32, 0, 0, 0, 0);
	//SDL_FillRect(o->bg, NULL, SDL_MapRGB(o->bg->format, normal_color.r, normal_color.g, normal_color.b));
	MenuOption_UpdateBg(o);
}

void MenuOption_Deinit(MenuOption * o)
{
	SDL_FreeSurface(o->bg);
}

void MenuOption_UpdateBg(MenuOption *o)
{
	if (o->selected) SDL_FillRect(o->bg, NULL, SDL_MapRGB(o->bg->format, o->normal_color.r, o->normal_color.g, o->normal_color.b));
	else SDL_FillRect(o->bg, NULL, SDL_MapRGB(o->bg->format, o->selected_color.r, o->selected_color.g, o->selected_color.b));
}